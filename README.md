# Уроки Python

## 1.  [Распределённая система контроля версий Git](1. Git.md)

1. [Что такое Git?](1. Git.md#1-что-такое-git)
2. [Основные возможности Git](1. Git.md#2-основные-возможности-git)
3. [Основные понятия Git](1. Git.md#3-основные-понятия-git)
4. [Пример слияния веток](1. Git.md#4-пример-слияния-веток)
5. [Установка git](1. Git.md#5-установка-git)
6. [Основы работы с командной строки/терминалом](1. Git.md#6-основы-работы-с-командной-строкой-терминалом)
7. [Создание тестового репозитория](1. Git.md#7-создание-тестового-репозитория)
8. [Изменение состояния, фиксация](1. Git.md#8-изменение-состояния-фиксация)
9. [Ветки](1. Git.md#9-ветки)
10. [Сервисы для хостинга репозиториев](1. Git.md#10-сервисы-для-хостинга-репозиториев)
11. [Использование удалённых репозиториев](1. Git.md#11-использование-удалённых-репозиториев)
12. [Где изучать Git](1. Git.md#12-где-изучать-git)

## 2.  [Введение в Python](2. Introduction in Python.md)

1. [Сферы применения Python](2. Introduction in Python.md#1-сферы-применения-python)
2. [История языка](2. Introduction in Python.md#2-история-языка)
3. [Что такое программа](2. Introduction in Python.md#3-что-такое-программа)
4. [Структура компьютера](2. Introduction in Python#4-структура-компьютера)
5. [Особенности языка программирования Python](2. Introduction in Python.md#5-особенности-языка-программирования-python)
6. [Философия языка](2. Introduction in Python.md#6-философия-языка)
7. [Установка интерпретатора Python](2. Introduction in Python.md#7-установка-интерпретатора-python)
8. [Режимы интерпретатора Python](2. Introduction in Python.md#8-режимы-интерпретатора-python)
9. [Hello, world! в интерактивном интепретаторе](2. Introduction in Python.md#9-hello-world-в-интерактивном-интерпретаторе)
10. [Hello, world! в модуле](2. Introduction inPython.md#10-hello-world-в-модуле)
11. [Установка IDE или редактора кода](2. Introduction in Python.md#11-установка-ide-или-редактора-кода)
12. [Формальные языки](2. Introduction in Python.md#12-формальные-языки)
13. [Основы синтаксиса Python](2. Introduction in Python.md#13-основы-синтаксиса-python)
14. [Переменные](2. Introduction in Python.md#14-переменные)
15. [Функции input и print](2. Introduction in Python.md#15-функции-input-и-print)
16. [Базовые типы данных](2. Introduction in Python.md#16-базовые-типы-данных)
17. [Операции с числами](2. Introduction in Python.md#17-операции-с-числами)
18. [Самостоятельная работа](2. Introduction in Python.md#18-самостоятельная-работа)
19. [Условные операторы](2. Introduction in Python.md#19-условные-операторы)
20. [Самостоятельная работа](2. Introduction in Python.md#20-самостоятельная-работа)
21. [Домашняя работа](2. Introduction in Python.md#21-домашняя работа)

## 3.  [Встроенные типы и простейшие операции над ними](3. Built-in types and the simplest operations on them.md)

1. [Коллекции](3. Built-in types and the simplest operations on them.md#1-коллекции)
2. [Понятие объекта](3. Built-in types and the simplest operations on them.md#2-понятие-объекта)
3. [Последовательности (итераторы)](3. Built-in types and the simplest operations on them.md#3-последовательности-итераторы)
4. [Строки](3. Built-in types and the simplest operations on them.md#4-строки)
5. [Списки](3. Built-in types and the simplest operations on them.md#5-списки)
6. [Кортежи](3. Built-in types and the simplest operations on them.md#6-кортежи)
7. [Индексация, срезы](3. Built-in types and the simplest operations on them.md#7-индексация-срезы)
8. [Самостоятельная работа](3. Built-in types and the simplest operations on them.md#8-самостоятельная-работе)
9. [Множества](3. Built-in types and the simplest operations on them.md#9-множества)
10. [Замороженные множества](3. Built-in types and the simplest operations on them.md#10-замороженные-множества)
11. [Словари](3. Built-in types and the simplest operations on them.md#11-словари)
12. [Вложенные списки (матрицы)](3. Built-in types and the simplest operations on them.md#12-вложенные-списки-матрицы)
13. [Домашнее задание](3. Built-in types and the simplest operations on them.md#13-домашнее-задание)

## 4.  [Циклы](4. Cycles.md)

1. [Цикл while](4. Cycles.md#1-цикл-while)
2. [Цикл for](4. Cycles.md#2-цикл-for)
3. [Оператор break](4. Cycles.md#3-оператор-break)
4. [Оператор continue](4. Cycles.md#4-оператор-continue)
5. [Функция range](4. Cycles.md#5-функция-range)
6. [Генераторы (comprehensions)](4. Cycles.md#6-генераторы-comprehensions)
7. [Вложенные циклы](4. Cycles.md#7-вложенные-циклы)
8. [Самостоятельная работа](4. Cycles.md#8-самостоятельная-работа)
9. [Домашняя работа](4. Cycles.md#9-домашняя-работа)

## 5.  [Функции](5. Functions.md)

1. [Понятие функции](5. Functions.md#1-понятие-функции)
2. [Роль функции в программировании](5. Functions.md#2-роль-функции-в-программировании)
3. [Встроенные функции интерпретатора Python](5. Functions.md#3-встроенные-функции-интерпретатора-python)
4. [Функции в Python](5. Functions.md#4-функции-в-Python)
5. [Лямбда-функции, анонимные функции](5. Functions.md#5-лямбда-функции-анонимные-функции)
6. [Scope (область видимости)](5. Functions.md#6-scope-область-видимости)
7. [Локальная область видимости](5. Functions.md#7-локальная-область-видимости)
8. [Глобальная область видимости](5. Functions.md#8-глобальная-область-видимости)
9. [Нелокальная область видимости)](5. Functions.md#9-scope-область-видимости)
10. [Передача изменяемых параметров](5. Functions.md#10-передача-изменяемых-параметров)
11. [Самостоятельная работа](5. Functions.md#11-самостоятельная-работа)
12. [Домашняя работа](5. Functions.md#12-домашняя-работа)

## 6.  [Изолированное окружение. Пакеты, модули](6. Isolated environment. Packages, modules.md)

1. [Модули и пакеты](6. Isolated environment. Packages, modules.md#1-модули-и-пакеты)
2. [Менеджер пакетов pip](6. Isolated environment. Packages, modules.md#2-менеджер-пакетов-pip)
3. [Virtualenv](6. Isolated environment. Packages, modules.md#3-virtualenv)
4. [Pipenv](6. Isolated environment. Packages, modules.md#4-pipenv)
5. [Домашняя работа](6. Isolated environment. Packages, modules.md#5-домашняя-работа)

## 7.  [Оформление кода](7. Code design.md)

1. [Что такое PEP](7. Code design.md#1-что-такое-pep)
2. [Внешний вид кода](7. Code design.md#2-внешний-вид-кода)
3. [Пробелы в выражениях и инструкциях](7. Code design.md#3-пробелы-в-выражениях-и-инструкциях)
4. [Комментарии](7. Code design.md#4-комментарии)
5. [Контроль версий](7. Code design.md#5-контроль-версий)
6. [Соглашение по именованию](7. Code design.md#6-соглашение-по-именованию)
7. [Общие рекомендации](7. Code design.md#7-обшие-рекомендации)
8. [Инструменты для проверки оформления кода](7. Code design.md#8-инструменты-для-проверки-оформления-кода)
9. [Оформление репозитория](7. Code design.md#9-оформление-репозитория)

## 8.  Контрольная работа №1

## 9.  [Файловый ввод/вывод, сериализация/десериализация](9. File input-output, serialization-deserialization.md)

1. [Символы перевода строки](9. File input-output, serialization-deserialization.md#1-символы-перевода-строки)
2. [Работа с файлами](9. File input-output, serialization-deserialization.md#2-работа-с-файлами)
3. [Режимы работы с файлами](9. File input-output, serialization-deserialization.md#3-режимы-работы-с-файлами)
4. [Указатель](9. File input-output, serialization-deserialization.md#4-указатель)
5. [Оператор with](9. File input-output, serialization-deserialization.md#2-оператор-with)
6. [Сериализация, десериализация](9. File input-output, serialization-deserialization.md#6-сериализация-десериализация)
7. [Pickle](9. File input-output, serialization-deserialization.md#7-pickle)
8. [JSON](9. File input-output, serialization-deserialization.md#8-json)
9. [YAML](9. File input-output, serialization-deserialization.md#9-yaml)
10. [Домашняя работа](9. File input-output, serialization-deserialization.md#10-домашняя-работа)

## 10. [ООП](10. OOP.md)

1. [Понятие ООП](10. OOP.md#1-понятие-ооп)
2. [Класс, объект](10. OOP.md#2-класс-объект)
3. [Методы, конструктор, деструктор, __new__](10. OOP.md#3-методы-конструктор-деструктор-\_\_new\_\_)
4. [Наследование](10. OOP.md#4-наследование)
5. [Двойное наследование](10. OOP.md#5-двойное-наследование)
6. [Полиморфизм](10. OOP.md#6-полиморфизм)

## 11. [Исключения](11. Exclusions.md)

1. [Понятие исключения](11. Exclusions.md#1-понятие-исключения)
2. [Генерация исключения](11. Exclusions.md#2-генерация-исключения)
3. [Перехват исключения](11. Exclusions.md#3-перехват-исключения)
4. [Стандартные исключения](11. Exclusions.md#4-стандартные-исключения)
5. [Создание класса исключения](11. Exclusions.md#5-создание-класса-исключения)
6. [Выброс исключения](11. Exclusions.md#6-выброс-исключения)

## 12. [Декораторы, итераторы, генераторы, слоты, метаклассы](12. Decorators, iterators, generators, slots, metaclasses.md)

1. [Декораторы](12. Decorators, iterators, generators, slots, metaclasses.md#1-декораторы)
2. [Итераторы](12. Decorators, iterators, generators, slots, metaclasses.md#2-итераторы)
3. [Генераторы](12. Decorators, iterators, generators, slots, metaclasses.md#3-генераторы)
4. [Слоты](12. Decorators, iterators, generators, slots, metaclasses.md#4-слоты)
5. [Метаклассы](12. Decorators, iterators, generators, slots, metaclasses.md#5-метаклассы)

## 13. [Процессы и потоки](13. Processes and streams.md)

1. [Общие понятия](13. Processes and streams.md#1-общие-понятия)
2. [Многопоточное программирование](13. Processes and streams.md#2-многопоточное-программирование)
3. [GIL](13. Processes and streams.md#3-gil)
4. [Мультпроцессное программирование](13. Processes and streams.md#4-мультипроцессное-программирование)
5. [Асинхронное программирование](13. Processes and streams.md#5-асинхронное-программирование)

## 14. [Графический интерфейс. Tkinter]

## 15. [Графический интерфейс. PyQT]

## 16. [Графический интерфейс. Практическое занятие]

## 17. [Введение в базы данных. Часть 1]

* [DDL, DML]
* [PEP 249 - Python Database API]
* [SQLite3 - Синтаксис SQL-запросов, CRUD]

## 18. [Введение в базы данных. Часть 2]

## 19. [Контрольная работа: графическое приложение для взаимодействия с БД]

## 20. [TCP/IP. Сокеты]

## 21. [HTTP/HTTPS]

## 22. [Web-разработка. Часть 1]

## 23. [Web-разработка. Часть 2]

## 24. [Web-разработка. Практическое занятие]

* [Кратко: стек технологий]
* [Реализация приложения "Регистрация на мероприятия"]

## 25. [Контрольная работа: web-разработка]

## 26. [Модуль click]

## 27. [Разработка игр. PyGame]

## 28. [Практическое занятие "Пишем игру в команде"]

## 29. [Введение в модульное тестирование]

## 31. [Защита курсовой работы]
